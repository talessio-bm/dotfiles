
#  ░░░░░░  ░░░░░░  ░░░    ░░ ░░░░░░░ ░░  ░░░░░░     ░░░░░░░ ░░ ░░░░░░░ ░░   ░░
# ▒▒      ▒▒    ▒▒ ▒▒▒▒   ▒▒ ▒▒      ▒▒ ▒▒          ▒▒      ▒▒ ▒▒      ▒▒   ▒▒
# ▓▓      ▓▓    ▓▓ ▓▓ ▓▓  ▓▓ ▓▓▓▓▓   ▓▓ ▓▓   ▓▓▓    ▓▓▓▓▓   ▓▓ ▓▓▓▓▓▓▓ ▓▓▓▓▓▓▓
# ▒▒      ▒▒    ▒▒ ▒▒  ▒▒ ▒▒ ▒▒      ▒▒ ▒▒    ▒▒    ▒▒      ▒▒      ▒▒ ▒▒   ▒▒
#  ░░░░░░  ░░░░░░  ░░   ░░░░ ░░      ░░  ░░░░░░  ░░ ░░      ░░ ░░░░░░░ ░░   ░░

# environment {{{
    set -gx LANG        en_US.UTF-8
    set -gx LC_COLLATE  en_US.UTF-8
    set -gx LC_CTYPE    en_US.UTF-8
    set -gx LC_MESSAGES en_US.UTF-8
    set -gx LC_MONETARY en_US.UTF-8
    set -gx LC_NUMERIC  en_US.UTF-8
    set -gx LC_TIME     en_US.UTF-8
    set -gx LC_ALL      en_US.UTF-8
    set -gx PATH $HOME/.bin /usr/local/bin $PATH
    set -gx EDITOR      vim
# }}}
# prompt {{{
    function fish_prompt # {{{
        set_color magenta ; echo '' ; echo -n (whoami)
        set_color normal  ; echo -n ' at '
        set_color yellow  ; echo -n (hostname -s)
        set_color normal  ; echo -n ' in '
        set_color green   ; echo -n (prompt_pwd)
        set branch (current_git_branch)
        if not test -z $branch
            set_color normal ; echo -n ' on '
            set_color cyan   ; echo -n $branch
        end
        set action (current_git_action)
        if not test -z $action
            set_color normal ; echo -n ' during '
            set_color red    ; echo -n $acton
        end
        set_color normal ; echo '' ; echo -n '>> '
    end
    # }}}
    function current_git_branch # {{{
        echo (git branch ^/dev/null|grep '*'|awk '{print $(NF)}'|sed 's/)//')
    end
    # }}}
    function current_git_action # {{{
        if      test -e $PWD/.git/BISECT_START ; echo 'bisect'
        else if test -e $PWD/.git/MERGE_HEAD   ; echo 'merge'
        else if test -e $PWD/.git/rebase-apply ; echo 'rebase'
        else if test -e $PWD/.git/rebase-merge ; echo 'rebase-i'
        end
    end
    # }}}
    function fish_mode_prompt # {{{
        # empty, showing vi mode on right prompt
    end
    # }}}
    function fish_right_prompt # {{{
        switch $fish_bind_mode
            case default ; set mode_color 'blue'  ; set mode_label 'normal'
            case visual  ; set mode_color 'red'   ; set mode_label 'visual'
            case insert  ; set mode_color 'green' ; set mode_label 'insert'
        end
        set_color $mode_color ; echo -n "-- $mode_label --" ; set_color normal
    end
    # }}}
# }}}
# vi mode {{{
    function fish_user_key_bindings # {{{
        fish_vi_key_bindings
        bind -M insert \cl 'clear; commandline -f repaint'
    end
    # }}}
    fish_vi_mode
    set fish_key_bindings fish_user_key_bindings
# }}}
# custom aliases {{{
    alias ll='ls -lah'     # list files on long format, hidden and human size
    alias mkdir='mkdir -p' # create intermediate directories as required
    alias vim='vim -X'     # don't connect to X server to shorten startup time
    alias vi='vim -X'      # always run vim, improved!
    alias tmux='tmux -2u'  # tmux with 256 colors and unicode support
# }}}
# tmux {{{
    if not set -q TMUX
        tmux -S ~/.my-tmux-session attach
        or tmux -S ~/.my-tmux-session
    end
# }}}

# vim:fdm=marker foldlevel=0:
