 
"    ░░    ░░ ░░ ░░░    ░░░ ░░░░░░   ░░░░░░
"    ▒▒    ▒▒ ▒▒ ▒▒▒▒  ▒▒▒▒ ▒▒   ▒▒ ▒▒
"    ▓▓    ▓▓ ▓▓ ▓▓ ▓▓▓▓ ▓▓ ▓▓▓▓▓▓  ▓▓
"     ▒▒  ▒▒  ▒▒ ▒▒  ▒▒  ▒▒ ▒▒   ▒▒ ▒▒
"  ░░  ░░░░   ░░ ░░      ░░ ░░   ░░  ░░░░░░

" general {{{
    set nocp                  " disable vi compatibility
    set nobk noswf            " no backups nor swap files
    let g:netrw_dirhistmax=0  " no history of recently visited directories
    set hi=999                " keep history of last 999 commands
    set enc=utf-8 fenc=utf-8  " default charset for vim and files
    set ffs+=mac              " if file format is not set yet, set as mac
    set ar                    " detects file changes outside vim
    set bs=2                  " backspace behavior (indent,eol,start)
    set mouse=                " no mouse
    set noto ttimeout ttm=200 " quickly timeout on keycodes, never on mappings
    set lz                    " only redraw screen when needed
    set ml mls=1              " accept modeline in files
    set nf-=octal             " don't threat numbers as octals on <c-a>/<c-x>
" }}}
" interface {{{
    set noeb novb t_vb=                  " be quiet, no bells nor flashes
    set spr sb                           " new splits open at right or bottom
    set nu rnu                           " line numbers relative to cursor
    set lcs=eol:¬,trail:.,tab:>-         " representation of invisible chars
    set lcs+=extends:>,precedes:<        " more invisible chars
    set fcs=vert:\│,fold:-               " chars used for interface elements
    set nosmd                            " hide mode (already on statusline)
    set sc                               " show partial commands on cmd line
    set confirm                          " ask confirmation before actions
    set so=999 siso=999                  " keep cursor at center of window
    set dy=lastline                      " don't replace long lines by '@'
    set fen fdm=indent foldlevel=9       " enable folding
    set wmnu wim=list:longest            " nice completion on command line
    set is hls ic scs                    " highlight search as you type
    set sm mps+=<:>                      " jump to match when insert bracket
    set cc=                              " highlight text width limit
    au InsertEnter  * call OnInsert()    " settings for insert mode
    au CursorMovedI * call UpdateFocus() " defines text region to be 'focused'
    au InsertLeave  * call OnNormal()    " settings for normal mode
    set stal=2 tal=%!MyTabLine()         " tabline is always visible
    set ls=2 stl=%!MyActiveStatusLine()  " statusline is always visible
    au WinEnter * call OnWinEnter()      " settings for active window
    au WinLeave * call OnWinLeave()      " settings for inactive windows

    fu! OnInsert() " {{{
        set  cul cuc list cc=+0 " visual elements relevant for insert mode
        call UpdateFocus()      " highlight text region around cursor
    endfunction
    " }}}
    fu! OnNormal() " {{{
        set  nocul nocuc nolist cc= " hide insert-mode visual elements
        call ClearFocus()           " switch off 'focus' region highlight
    endfunction
    " }}}
    fu! UpdateFocus() " {{{
        call ClearFocus()
        let  r     = 6
        let  start = (line('.') - r > 0 ? line('.') - r : 0)
        let  end   = (line('.') + r < line('$') ? line('.') + r : line('$'))
        call SetFocus(start, end)
    endfunction
    " }}}
    fu! ClearFocus() " {{{
        while exists('w:dim_match_ids') && !empty(w:dim_match_ids)
            silent! call matchdelete(remove(w:dim_match_ids, -1))
        endwhile
    endfunction
    " }}}
    fu! SetFocus(start, end) " {{{
        let  w:dim_match_ids = get(w:, 'dim_match_ids', [])
        call add(w:dim_match_ids, matchadd('Dim','\%<'.a:start.'l',10))
        call add(w:dim_match_ids, matchadd('Dim','\%>'. a:end .'l',10))
    endfunction
    " }}}
    fu! MyTabLine() " {{{
        let line = '' | let i = 1
        while i <= tabpagenr('$')
            let line.= '%#TabLine'.(i == tabpagenr() ? 'Sel' : '').'# '
            let line.= GetCurrentTabLabel(i)
            let line.= (i == tabpagenr()) ? ' ░▒▓' : '    '
            let i+=1
        endwhile
        return line.'%#TabLineFill#%='
    endfunction
    " }}}
    fu! GetCurrentTabLabel(i) " {{{
        let file = tabpagebuflist(a:i)[tabpagewinnr(a:i)-1]
        let file = pathshorten(bufname(file))
        let file = (file == '') ? '[No Name]' : file
        return a:i.':'.file
    endfunction
    " }}}
    fu! OnWinEnter() " {{{
        call ClearFocus()
        setlocal stl=%!MyActiveStatusLine()
    endfunction
    " }}}
    fu! OnWinLeave() " {{{
        if !&diff
            call SetFocus(0,0)
        endif
        setlocal stl=%!MyInactiveStatusLine()
    endfunction
    " }}}
    fu! MyActiveStatusLine() "{{{
        return GetCurrentMode().GetCommonStatusLineElements('1')
    endfunction
    " }}}
    fu! MyInactiveStatusLine() " {{{
        return GetCommonStatusLineElements('2')
    endfunction
    " }}}
    fu! GetCommonStatusLineElements(c) " {{{
        return ' %t %m%r%h%w%=%'.a:c.'*▓▒░ type:%{&ft} │ pos:%l:%v │ len:%L '
    endfunction
    " }}}
    fu! GetCurrentMode() " {{{
        if     mode()==#'i'     | hi User1 ctermbg=156 | let label = 'insert'
        elseif mode()==#'R'     | hi User1 ctermbg=140 | let label = 'replace'
        elseif mode()==#'v'     | hi User1 ctermbg=210 | let label = 'visual'
        elseif mode()==#'V'     | hi User1 ctermbg=210 | let label = 'v.line'
        elseif mode()=="\<C-v>" | hi User1 ctermbg=210 | let label = 'v.block'
        else                    | hi User1 ctermbg=153 | let label = 'normal'
        endif
        return '%1* '.label.(&paste?' (paste)':'').' ░▒▓%0*'
    endfunction
    " }}}
" }}}
" editing {{{
    set ai si et sta sr ts=4 sw=4 sts=4 " auto indentation
    set nowrap tw=78                    " max length for lines
    set ve=all                          " allow moving cursor anywhere
    set nospell spl=en_us               " spell checking language
    set fo=tcqnj                        " format options (check :h fo-table)

    " indentation preferences by filetype {{{
        au Filetype ruby setlocal ts=2 sts=2 sw=2
        au Filetype yaml setlocal ts=2 sts=2 sw=2 noai nosi
    " }}}
" }}}
" completion {{{
    set ofu=syntaxcomplete#Complete " enable code completion
    set cot=longest,menuone         " match longest common, always show menu
    set dict=/usr/share/dict/words  " path to dictionary
    set complete+=k                 " include dictionary on <c-n> suggestions
" }}}
" colors {{{
    set t_Co=256 " use 256 colors
    syn enable   " enable syntax highlighting
    set bg=dark  " adjust colors for dark backgrounds

    " general {{{
        hi Normal          ctermfg=255 ctermbg=234
        hi Comment         ctermfg=245
        hi Todo            ctermfg=251
        hi Title           ctermfg=113
        hi Constant        ctermfg=209
        hi Special         ctermfg=107
        hi Delimiter       ctermfg=067
        hi String          ctermfg=107
        hi StringDelimiter ctermfg=064
        hi Identifier      ctermfg=183
        hi Structure       ctermfg=117
        hi Function        ctermfg=222
        hi Statement       ctermfg=105
        hi PreProc         ctermfg=117
        hi Type            ctermfg=215
        hi SpecialKey      ctermfg=238 ctermbg=234
        hi Directory       ctermfg=186
        hi! link Operator Structure
    " }}}
    " interface elements {{{
        hi NonText      ctermfg=241
        hi Dim          ctermfg=238 cterm=none
        hi Visual       ctermbg=238
        hi Cursor       ctermfg=233 ctermbg=153
        hi CursorLine   ctermbg=236 cterm=none
        hi CursorColumn ctermbg=236 cterm=none
        hi LineNr       ctermfg=238 ctermbg=234
        hi CursorLineNr ctermfg=244 ctermbg=234
        hi Folded       ctermfg=244 ctermbg=236
        hi FoldColumn   ctermfg=241 ctermbg=236
        hi SignColumn   ctermfg=243 ctermbg=237
        hi ColorColumn  ctermbg=161 cterm=none
        hi VertSplit    ctermfg=236 cterm=none
        hi Pmenu        ctermfg=231 ctermbg=241
        hi PmenuSel     ctermfg=233 ctermbg=255
        hi WildMenu     ctermfg=117 ctermbg=053
        hi Search       ctermfg=117 ctermbg=053
        hi MatchParen   ctermfg=231 ctermbg=067
        " messages {{{
            hi ErrorMsg ctermbg=088
            hi Question ctermfg=077
            hi! link Error  ErrorMsg
            hi! link MoreMsg Special
        " }}}
        " spell checking {{{
            hi SpellBad   ctermbg=088
            hi SpellCap   ctermbg=020
            hi SpellRare  ctermbg=053
            hi SpellLocal ctermbg=023
        " }}}
        " tabline {{{
            hi TabLine     ctermfg=242 ctermbg=235 cterm=none
            hi TabLineFill ctermbg=235 cterm=none
            hi TabLineSel  ctermfg=236 ctermbg=231 cterm=none
        " }}}
        " statusline {{{
            hi StatusLine   ctermfg=255 ctermbg=235 cterm=none
            hi StatusLineNC ctermfg=240 ctermbg=235 cterm=none
            hi User1        ctermfg=236 ctermbg=153 cterm=none
            hi User2        ctermfg=235 ctermbg=238 cterm=none
        " }}}
    " }}}
    " diff {{{
        hi! link diffRemoved Constant
        hi! link diffAdded   String
    " }}}
    " vimdiff {{{
        hi DiffAdd    ctermfg=193 ctermbg=022
        hi DiffDelete ctermfg=052 ctermbg=088
        hi DiffChange ctermbg=024
        hi DiffText   ctermfg=117 ctermbg=016 cterm=reverse
    " }}}
    " php {{{
        hi StorageClass ctermfg=180
        hi! link phpFunctions   Function
        hi! link phpSuperglobal Identifier
        hi! link phpQuoteSingle StringDelimiter
        hi! link phpQuoteDouble StringDelimiter
        hi! link phpBoolean     Constant
        hi! link phpNull        Constant
        hi! link phpArrayPair   Operator
        hi! link phpOperator    Normal
        hi! link phpRelation    Normal
        hi! link phpVarSelector Identifier
    " }}}
    " python {{{
        hi! link pythonOperator Statement
    " }}}
    " ruby {{{
        hi rubyClass                ctermfg=031
        hi rubyIdentifier           ctermfg=147
        hi rubyInstanceVariable     ctermfg=147
        hi rubySymbol               ctermfg=068
        hi rubyControl              ctermfg=068
        hi rubyRegexpDelimiter      ctermfg=053
        hi rubyRegexp               ctermfg=162
        hi rubyRegexpSpecial        ctermfg=126
        hi rubyPredefinedIdentifier ctermfg=204
        hi! link rubySharpBang              Comment
        hi! link rubyConstant               Type
        hi! link rubyFunction               Function
        hi! link rubyGlobalVariable         rubyInstanceVariable
        hi! link rubyModule                 rubyClass
        hi! link rubyString                 String
        hi! link rubyStringDelimiter        StringDelimiter
        hi! link rubyInterpolationDelimiter Identifier
    " }}}
    " erlang {{{
        hi! link erlangAtom      rubySymbol
        hi! link erlangBIF       rubyPredefinedIdentifier
        hi! link erlangFunction  rubyPredefinedIdentifier
        hi! link erlangDirective Statement
        hi! link erlangNode      Identifier
    " }}}
    " javascript {{{
        hi! link javaScriptValue        Constant
        hi! link javaScriptRegexpString rubyRegexp
    " }}}
    " coffeescript {{{
        hi! link coffeeRegExp javaScriptRegexpString
    " }}}
    " lua {{{
        hi! link luaOperator Conditional
    " }}}
    " c {{{
        hi! link cFormat   Identifier
        hi! link cOperator Constant
    " }}}
    " objective-c/cocoa {{{
        hi! link objcClass       Type
        hi! link cocoaClass      objcClass
        hi! link objcSubclass    objcClass
        hi! link objcSuperclass  objcClass
        hi! link objcDirective   rubyClass
        hi! link objcStatement   Constant
        hi! link cocoaFunction   Function
        hi! link objcMethodName  Identifier
        hi! link objcMethodArg   Normal
        hi! link objcMessageName Identifier
    " }}}
    " vimscript {{{
        hi! link vimOper Normal
    " }}}
    " html {{{
        hi! link htmlTag     Statement
        hi! link htmlEndTag  htmlTag
        hi! link htmlTagName htmlTag
    " }}}
    " xml {{{
        hi! link xmlTag             Statement
        hi! link xmlEndTag          xmlTag
        hi! link xmlTagName         xmlTag
        hi! link xmlEqual           xmlTag
        hi! link xmlEntity          Special
        hi! link xmlEntityPunct     xmlEntity
        hi! link xmlDocTypeDecl     PreProc
        hi! link xmlDocTypeKeyword  PreProc
        hi! link xmlProcessingDelim xmlAttrib
    " }}}
" }}}
" references {{{
    " jellybeans - https://github.com/nanotech/jellybeans.vim
    " i simplified the original colorscheme since i just need it on 256 colors
    " and also slight color approximations adjustments.

    " limelight - https://github.com/junegunn/limelight.vim
    " the original plugin is much smarter to identify text regions, my version
    " is very minimal. i also used this idea to fade out all inactive splits.

    " xero - http://code.xero.nu/dotfiles
    " the dotfiles of this guy inspired me a lot, he is a nice ascii artist ;D
" }}}

" vim:fdm=marker foldlevel=0:
